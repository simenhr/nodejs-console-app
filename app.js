const readline = require('readline');
const fs = require('fs');
const os = require('os');
const http = require('http');
const server = http.createServer((req, res) => {
    if (req.url === '/') {
        res.write('Hello World');
        res.end();
    }
});
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let num = 0;

console.log(`Choose an option:
1. Read package.json
2. Display OS info
3. Start HTTP server`)

rl.question('Type a number: ', function(number){
    num = number;
    if(number === '1'){
        fs.readFile(__dirname + '/package.json', 'utf-8', (err, content) => {
            if (err) throw err;
            let package = JSON.parse(content);
            console.log(content);
            rl.close();
        })
    }

    if(number === '2'){
        console.log('Getting OS info...');
        const systemMemory = () => {
        return (os.totalmem()/1024/1024/1024).toFixed(2);
        }
        const freeMemory = () => {
            return (os.freemem()/1024/1024/1024).toFixed(2);
        } 
        const cpuCores = os.cpus().length;
        const arch = os.arch();
        const platform = os.platform();
        const release = os.release();
        const user = os.userInfo().username;
        console.log(`
        SYSTEM MEMORY: ${systemMemory()} GB
        FREE MEMORY: ${freeMemory()} GB
        CPU CORES: ${cpuCores}
        ARCH: ${arch}
        PLATFORM: ${platform}
        RELEASE: ${release}
        USER: ${user}`);
        rl.close();
    }

    if(number === '3') {
        
        server.listen(3000);

        console.log('lsitening on port 3000...');

        }

    if(number !== '1' && number !== '2' && number !== '3'){
        console.log('invalid option chosen');
        rl.close();
    }
});



